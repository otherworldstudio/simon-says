﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class ColorButton : MonoBehaviour
    {
        public bool isHighlighted;
        private Button self;
        public float dimmingTime = 1f;
        public int id;
        public SimonSays instance;

        private void Start()
        {
            self = GetComponent<Button>();
            self.onClick.AddListener(() => highlightBtn());
        }

        public void highlightBtn()
        {
            if (!instance.IsSimonsTurn)
            {
                if (instance.sequence[instance.playerSequence.Count] == id)
                {
                    instance.playerSequence.Add(id);
                }
                else
                {
                    instance.GameOver = true;
                }
            }
            StartCoroutine(highlighterTest());
        }

        IEnumerator highlighterTest()
        {
            Highlight();
            yield return new WaitForSeconds(dimmingTime);
            Dim();
        }
        
        public void Highlight()
        {
            float H, S, V;
            Color.RGBToHSV(self.image.color, out H, out S, out V);

            V = 1f;

            var newColor = Color.HSVToRGB(H, S, V);

            self.image.color = newColor;
        }

        public void Dim()
        {
            float H, S, V;
            Color.RGBToHSV(self.image.color, out H, out S, out V);

            V = 0.5f;

            var newColor = Color.HSVToRGB(H, S, V);

            self.image.color = newColor;
        }
    }
}
