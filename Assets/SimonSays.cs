﻿using Assets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimonSays : MonoBehaviour
{

    public ColorButton Btn1, Btn2, Btn3, Btn4, Btn5, Btn6, Btn7, Btn8, Btn9;
    public Text InfoText;
    public GameObject InfoPanel;

    public float globalDimmingTime = 1f;
    public float difficultySpeedModifier = 0.75f;

    public List<ColorButton> gameButtons;
    public List<int> sequence;
    public List<int> playerSequence;

    public int sequenceLimit = 5;

    public bool IsSimonsTurn = true;

    public bool newTurn = false;

    public bool GameOver = false;
    public GameObject GameOverPanel;
    public Text GameOverElementsText;

    private void Start()
    {
        gameButtons.Add(Btn1);
        gameButtons.Add(Btn2);
        gameButtons.Add(Btn3);
        gameButtons.Add(Btn4);
        gameButtons.Add(Btn5);
        gameButtons.Add(Btn6);
        gameButtons.Add(Btn7);
        gameButtons.Add(Btn8);
        gameButtons.Add(Btn9);

        UpdateDimmingTime(globalDimmingTime);

        newTurn = true;
    }

    private void Update()
    {
        if (!GameOver)
        {
            if (newTurn)
            {
                if (IsSimonsTurn)
                {
                    StartCoroutine(SimonsTurn());
                }
                else
                {
                    StartCoroutine(PlayersTurn());
                }
                newTurn = !newTurn;
            }
        }
        else
        {
            GameOverPanel.SetActive(true);
            GameOverElementsText.text = sequence.Count.ToString();
        }
    }

    IEnumerator PlayersTurn()
    {
        SetButtonState(true);
        InfoText.text = "Player does:";
        InfoPanel.GetComponent<Image>().color = Color.green;
        
        while(playerSequence.Count < sequence.Count)
        {
            yield return new WaitForSeconds(0);
        }

        globalDimmingTime *= difficultySpeedModifier;
        UpdateDimmingTime(globalDimmingTime);
        playerSequence.Clear();
        sequenceLimit++;
        newTurn = true;
        IsSimonsTurn = true;
        yield return new WaitForSeconds(globalDimmingTime + 0.2f);
    }

    IEnumerator SimonsTurn()
    {
        SetButtonState(false);
        InfoText.text = "Simon says:";
        InfoPanel.GetComponent<Image>().color = Color.red;

        yield return new WaitForSeconds(1f);
        if (sequence.Count > 0)
        {
            for(int i = 0; i < sequenceLimit-1; i++)
            {
                gameButtons[sequence[i]-1].highlightBtn();

                yield return new WaitForSeconds(globalDimmingTime + 0.2f);
            }
            var nextBtn = Random.Range(0, 9);

            gameButtons[nextBtn].highlightBtn();

            sequence.Add(gameButtons[nextBtn].id);
        }
        else
        {
            while (sequence.Count < sequenceLimit)
            {
                var nextBtn = Random.Range(0, 9);

                gameButtons[nextBtn].highlightBtn();

                sequence.Add(gameButtons[nextBtn].id);
                yield return new WaitForSeconds(globalDimmingTime + 0.2f);

            }
        }
        newTurn = true;
        IsSimonsTurn = false;
        yield return new WaitForSeconds(globalDimmingTime + 0.2f);
    }

    public void UpdateDimmingTime(float dimmingTime)
    {
        foreach (var btn in gameButtons)
        {
            btn.dimmingTime = dimmingTime;
        }
    }

    public void SetButtonState(bool state)
    {
        foreach (var btn in gameButtons)
        {
            btn.GetComponent<Button>().interactable = state;
        }
    }
}
